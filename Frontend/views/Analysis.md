# Aplikace iFin 2 - FrontEnd - View Analysis.vue

Jedná se o parent parent kompomentu pro vykreslení anaýly. Do této komonenty se vykreslují jednotlivé taby analýzy.

Komponenta načítá **AnalysisNav.vue**, což je navigace analýzy.

Kompoementa obsahuje metodu **checkBeforeSave()** která slouží pro ověřování, zda je uživatel autentizován, při ukládádní jendotlivých child komponent.

```js
    let vm = this;
    // return path, kterou je nutné poslat na autentizační endpoint API
    let return_path = window.location.pathname;

    // odeslání requestu na vlastní endpoint, zajišťující kominuikaci s API
    axios.post('/checkAuth', {
      return_path: return_path
    }).then(function(response) {
        // request je success, ale nevrátil žádná data (autentzace není platná)
      if(response.data != true && response.data != "not"){
        window.location.href = response.data;
      }
    }).catch(function(error) {
    // request není success
      if(typeof error.response !== "undefined" &&error.response.status == 419) {
        location.reload();
      } else {
        vm.$raven.captureException(error)
      }
    });
```



