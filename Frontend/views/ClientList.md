# Aplikace iFin 2 - FrontEnd - ClientList

View pro výpis klientů. Při načtení se pošle request pro získání všech klientů přihlášeného agenta.

**Výpis nejdůležitejších metod:**

- **setSpecificClient(clientID, nextUrl)** - metoda, která se volá po kliknutí na méno klienta. Metoda zjistí zvolení klienta a zobrazení jeho analýzy.

- **hideClient(index, row)** - metoda, která se volá po kliknutí na smazat klienta. Reálně klienta nesmaže, ale pouze skryje na straně API.

- **newClient()** - metoda, jež emmituje event na parent kompomemntu. Dojde k přesměrování na PersonalInfo bez zvoleného kleinta.

- **donwloadPdf()** - metoda pro stažení souhrnného PDF (PDF s daty analýzy)
- **shortPdf()** - metoda pro stažení krátkého PDF (PDF s daty analýzy)
- **getPdf()** - metoda pro stažení autorizačního protokolu, pokud je klient autorizován