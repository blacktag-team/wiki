# Aplikace iFin 2 - FrontEnd - Search view

Úvodní view celé aplikace, obsahující vyhledávání klentů.

Vyhledávání je možné podle jména, příjmení a emailu.

Vyhledávání je realizováno voláním metody **find()**, která vyhledává v již získaném poli klientů. Pole klientů je získáváno při načtení view, odelsáním requestu na interní endpoint, zajiš'tující komunikaci s externím API.

**Metoda pro získání všch klientů**

```js
beforeMount() {
    let vm = this;
    vm.$store.commit('setLoading', true);

    axios.get('/allClients')
    .then(function (response) {
      console.log(response.data);
      vm.AllClients = response.data.clients;
      vm.dataReady = true;
      vm.$store.commit('setLoading', false);
    })
    .catch(function (error) {
      vm.$raven.captureException(error)
    });
}
```

**Metoda pro vyhledávání**

```js
find() {
    let vm = this;
    if(vm.search){
        vm.search = vm.search.toLowerCase();
        let result = [];
        vm.AllClients.forEach(function(elm, index) {
          if(vm.searchTypeVal == 'name'){
            if(elm.firstName.toLowerCase().indexOf(vm.search) >=0 ){
              result.push(elm);
            } 
          } else if(vm.searchTypeVal == 'surname') {
            if(elm.lastName.toLowerCase().indexOf(vm.search) >= 0){
              result.push(elm);
            } 
          } else {
            if(elm.email){
              if(elm.email.toLowerCase().indexOf(vm.search) >= 0){
                result.push(elm);
              } 
            }
          }
        });
        vm.clients = result;
    }
}

```

Pokud uživatel klikne na klienta ve výsledku vyhledávání, dostane se na analýzu klienta (zajištěno metodou **sessionSet()**)