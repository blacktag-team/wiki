# Aplikace iFin2 - FrontEnd - Autorization view

View sloužíví pro autorizaci klienta. 

**Kroky jsou následující:**

1. Ověření, zda byly zakliknuty souhlasy - zavolá se metoda **checkedTest()**
2. Odeslání sms na tel. klienta - zavolá se metoda **sendSms()**
3. Vyplnění zasaného SMS kódu a ověření správnosti - zavolá se metoda **authBySms()**
4. Zpřístupnění autorizačního PDF - zavolá se metoda **getPdf()**, která vytovří formulář. **Tento postup je nutný, jelikož jinak nefunguje správnost názvů při stahování PDF.** 





# Metody #

sendSms() 

```js

sendSms() {
    // ověřím, zda jsem autentizován
    this.$emit('checkBeforeSave');
    let vm = this;
    // zapnu loading
    vm.$store.commit('setLoading', true);
    vm.responseMsg = '';
    // request na interní endpoint pro komunikaci s externím API
    axios.get('/sendsms')
    .then(function (response) {
        // sms odeslána
        if(response.data.sms == true){
            vm.sms = true;
            vm.smsinput = false; // skrýt tlačítko odeslat sms 
            vm.autorizaceInput = true; // zobrazit input pro zadání SMS kódu

            // scroll a focus na input pro zadání kódu 
            setTimeout(function(){
                vm.$scrollTo('#sms-col', 1000);
                document.getElementById('code').focus();
            }, 300);

        } else if(response.data == "SMS již byla vyžádána."){
          vm.$store.commit('modalHeading', response.data);
          vm.$store.commit('modalContent', "SMS již byla vyžádána. Počkejte prosím 1 minutu pro zaslání nové.");
          vm.$store.commit('modalOpen', true);
        } else {
          vm.$store.commit('modalHeading', "Vyskytla se chyba");
          vm.$store.commit('modalContent', response.data);
          vm.$store.commit('modalOpen', true);
        }
        vm.$store.commit('setLoading', false);
      })
      .catch(function (error) {
        vm.$raven.captureException(error)
      });
    },
```

checkedTest()

```js
checkedTest() {
    let vm = this;
    if(this.partnerSet){
        if(this.souhlasimObchod && this.souhlasimOsobUdaje){
            vm.smsinput = true;
        } else {
            vm.smsinput = false;
        }
    } else {
        if(this.souhlasimObchod){
            vm.smsinput = true;
        } else {
            vm.smsinput = false;
        }
    }
}
```
authBySms()

```js
authBySms() {
    // ověřím, zda jsem autentizován
    this.$emit('checkBeforeSave');
    let vm = this;
    // zapnu loading
    vm.$store.commit('setLoading', true);
    // request na interní endpoint
    if(this.smsCode){
        axios.post('/authbysms', {
            code: this.smsCode
        })
        .then(function (response) {
            if(response.data == "Klient verifikován."){
                vm.$store.commit('modalHeading', 'Klient verifikován');
                vm.$store.commit('modalContent', 'Klient byl úspěšně ověřen. Klikněte, prosím, na tlačítko Stáhnout PDF a vytiskněte Protokol o elektronické komunikaci. Ten dejte podepsat klientovi a pošlete na BackOffice.');
                vm.$store.commit('modalType', 'autorizace');
                vm.$store.commit('modalOpen', true);

                vm.autorizaceInput = false;
                vm.pdf = true;
            } else {
                vm.responseMsg = response.data;
            }

            vm.$store.commit('setLoading', false);
        })
        .catch(function (error) {
            vm.$raven.captureException(error)
        });

    } else {
        vm.$store.commit('modalHeading', 'Vyskytla se chyba');
        vm.$store.commit('modalContent', 'Vyplňte prosím autorizační kód.');
        vm.$store.commit('modalOpen', true);

        vm.$store.commit('setLoading', false);
      }
    }
  },
```
getPdf()

```js
getPdf() {
    // ověřím, zda jsem autentizován
    this.$emit('checkBeforeSave');
    // vytvořím form
    var form = document.createElement("form");
    // nastavím atributy
    form.setAttribute("method", "post");
    form.setAttribute("action", "/autorizacni-protokol/"+this.$store.state.clientName+'-autorizacni-protokol');
    form.setAttribute("target", "_blank");
    // přidám inputy a values
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("name", "clientID");
    hiddenField.setAttribute("value",this.$store.state.clientID );
    hiddenField.setAttribute("type", "hidden");
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    // odešlu form a smažu jej
    form.submit();
    form.remove();
    }
```