# Aplikace iFin2 - FrontEnd - App view

Parent view pro celou SPA. SLouží pro vykreslení celé aplikace pomocí routes.

App načítá hlavní navigaci aplikace, která emmituje metody, jež jsou spracovány na straně App.

**Jedná se o tyto metody:**

sessionReset() - metoda pro odhlášení aktuálně zvoleného klienta

sessionResetAgent() - metoda pro odlášení agenta, jež byl zvolen z manažerského pohledu

**Další metody v App:**

getBrowser() - metoda, pro konntrolu použitého prohlížeče a případné vypsání hlásky o nepodporovaném prohlížeči