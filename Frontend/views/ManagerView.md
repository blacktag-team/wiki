# Aplikace iFin 2 - FrontEnd - ManagerView

View Manažerský pohled - zobrazuje přehled podřízených. View vypisuje jednotlivé řádky tabulky pomocí child kompomenty ManagerViewRow.

Přinačtení view je volán request na získání podříených přihlášeného agenta:

```js
// nejprve zjistím přihlášeního agenta
axios.get('/agentname')
.then(function (response) {
    vm.agentname = response.data.name;
    vm.agentID = response.data.ID;
    // pokud je zovleno datum pro filtraci záznamů
    if(vm.$route.params.date){
        vm.date = vm.$route.params.date;
        vm.setDate();
        vm.$store.commit('setLoading', false);
    } else {
        // získání dat o struktuře agenta
        axios.get('/managerdata')
          .then(function (response) {
            if(response.data != false){
              vm.input.clients = [];
              vm.input.clients.push(response.data);
            }   
            vm.dataReady = true
            vm.$store.commit('setLoading', false);
          })
          .catch(function (error) {
            vm.$raven.captureException(error)
        });   
    }
})
.catch(function (error) {
    vm.$raven.captureException(error)
}); 
```

Každý řádek struktury agenta je klikatelný a vede na detailní výpis daného podřízeného. Detail podřízenéh zobrazuje procentuální vyplněnost jednotlivých tabů analýzy pro kleitny jendotlivých podřízených. 

Kliknutí na řádek podřízeného již v detailu, zapříčiní přihlášneí za zvoleného agenta a tím přístup k jeho klientům.




