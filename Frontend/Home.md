# Aplikace iFin 2 - Frontend

Aplikace je vytvořena formou tzv. SPA (single page aplikace) a její frontend je kompletně napsán pomocí VueJS.

Jako podpůrné moduly pro zajištění požadovanéch chodu je používané následující:

- chart.js: ^2.7.2
- element-ui: ^2.3.4
- html2canvas: ^1.0.0-alpha.12
- normalize-scss: ^7.0.1
- nouislider: 10.1.0
- raven-js: ^3.26.2
- signature_pad: ^3.0.0-beta.3
- sortablejs: ^1.8.3
- vee-validate: ^2.0.9
- vue-analytics: ^5.16.1
- vue-chartjs: ^3.3.2
- vue-lodash: ^2.0.0
- vue-moment: ^3.2.0
- vue-numeric: ^2.3.0
- vue-raven: ^1.0.0
- vue-router: ^3.0.1
- vue-scrollto: ^2.11.0
- vue-slider-component: ^2.6.2
- vue-the-mask: ^0.11.1
- vue2-dropzone: ^3.5.7
- vuex: ^3.0.1

Jednotlivé části aplikace jsou rozděleny do views a komponent. Jednotlivé views jsou pak následující:

# Taby analýzy #
- Housing.vue
- Products.vue
- Income.vue
- ProductsNew.vue
- Investment.vue
- Rating.vue
- Rent.vue
- Risk.vue
- Costs.vue          
- PersonalInfo.vue 
- Family.vue    
- Priority.vue   

# Ostatní views

- [Analysis.vue](./views/Analysis.md)
- [App.vue](./views/App.md)
- [Authorization.vue](./views/Authorization.md)
- [ManagerView.vue](./views/ManagerView.md)      
- [ClientList.vue](./views/ClientList.md)      
- [Search.vue](./views/Search.md)

Komponenty jsou následující:

- **AnalysisNav.vue** - kompomenta složící pro navigaci mezi jednotlivými části analýzy
- **ManagerViewRow.vue** - jednotlivý řádek tabulky v manažerškém pohledu
- **BarChart.vue** - kompomenta pro vykreslování grafů
- **Modal.vue** - komponenta pro vyskakovací okno v případě chyby, nebo jiné události
- **Child.vue** - komponenta pro přidání dítěte
- **ModalPriority.vue** - modal pro přidání priority
- **DoughnutChart.vue** - komponenta pro donut chart
- **RecRating.vue** - položka doporučeného kontaktu v hodnocení a doporučení
- **HeaderNav.vue** - hlavní navigace
- **RecReality.vue** - položka doporučeného kontaktu pro prodej nemovitosti v hodnocení a doporučení
- **HousingGoal.vue** - komponenta pro cíle v bydlení
- **RiskPerson.vue** - komponenta pro přidání osoby v zajištění životního majetku a standardu
- **InvestmentRow.vue** - položka investice v záložce investice
- **RiskProperty.vue** - položka majetku v zajištění životního majetku a standardu
- **ManagerViewDetailRow.vue**  - jednotlivý řádek tabulky v detialu manažera - Manažerský pohled