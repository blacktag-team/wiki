# Aplikace iFin 2 - Backend - ManagerController

Controller slouží pro obsluhu manažerského pohledu.

**Obsahuje tyto metody:**

- **getManagerData(Request $request)** - metoda pro zísákní struktury daného agenta
- **postManagerData(Request $request)** - metoda pro získání dat detailu zvoleného agenta ze struktury
- **getXLS(Request $request)** - export do XLS **(zatím nedokončeno)**