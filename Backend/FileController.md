# Aplikace iFin 2 - Backend - FileController

Controller pro správu souborů, které se nahrávají v PesonalInfo a v produktech

- **upload(Request $request, $type, $guid = false)**

    - pukud je $type = product, tak je potřeba zaslat i $guid. GUID slouží pr opřiřazení souboru ke konkrétnímu produktu
    - metoda slouží pro nahrání souboru pomocí externího API
- **delete(Request $request, $type)**

    - metoda pro smazání nahranáho souboru
- **load(Request $request, $name)**

    - metoda pro načtení již nahraného souboru
- **convert(Request $request)**

    - metoda pro převod souboru do base64 pro následný upload