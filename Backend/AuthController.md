# Aplikace iFin 2 - Backend - AuthController

Controller pro autentizaci vůči externímu API. Autentizace je dle standardu oAuth2. 

**Metody controlleru:**

- **checkAuth(Request $request, $url = '')** 
    - metoda ověří platnost oAuth tokenů
    - následně ověří, zda server, kde apliakce běží není na localhostu, pokud ano, neověřuje dále autentizaci a rovnou vrátí údaje testovacího agenta
    - následně se ověřuje, zda existuje autentizační token a zda není po expiraci
    - v případě potřeby se zavolá metoda **refreshToken($token)**
    - pokud refresh token nevrátí success, nebo žádný token nemáme, posíláme request na autentizační API

- **hasCode(Request $request)** - metoda, která ověřuje, zda se z autentizačního API vrátil token, který je nutné převést na access token. Pokud se vrátil token, zavoláme metodu **checkCode($code)**
- **checkCode($code)** - metoda pošle request na autentizační api, spolu s tokenem, pro získání access tokenu. Token se následně uloží do session.

- **refreshToken()** - metoda pro produložení platnosti access tokenu
- **getSession()** - metoda pro získání aktuálního obsahu session
- **flushSession()** - smazání aktuální session