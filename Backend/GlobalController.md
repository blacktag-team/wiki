# Aplikace iFin 2 - Backend - GlobalController

Controlle obshauje globální metody, které jsou přístupné ve většině ostatních controllerů.

# Jedná se o tyto metody: #

**sendRequest($type, $endpoint, $data, $accept = 'application/json', $content_type = 'application/json')** 
- obecná metoda pro zasílání requestů na externí API.
- $type - typ requestu (POST, GET)
- $data - data, které se mají poslat
- $accept - parametr do headeru requestu 
- $content_type - parametr do headeru requestu

**resetSession(Request $request)**

- metoda pro smazání parametru header v sesson, který obahuje akutální informace o agentovi a klientovi

**resetSessionAgent(Request $request)**

- metoda, smaže loggedAs a loggedAsName ze session, což jsou hodnoty používané v případě přihlášení za podřízeného (z manažerského pohledu)

**setSession(Request $request)**

- metoda zavolá changeHeader()

**setSessionAgent(Request $request)**

- metoda, sloužící pro nastavení přihlášení podřízeného (z manažerského pohledu)

**getClient(Request $request)**

- metoda vrátí paramtr client z headeru uloženého v session.

**getLoggedAgent(Request $request)**

- metoda vrátí údaje aktuálně přihlášeního agenta a zohleďnuje také, pokud je agent přihlášen za podřízeného

**getAgentPhoto(Request $request)**

- vrátí fotku poradce v base64

**generateTokens()**

- metoda slouží pro vygenerování bezpečnostního tokenu (Bearer token)

**getHeader()**

- vráci celý header ze session

**changeHeader($response, $request)**

- nastaví header v session, používá se při vytvoření nového klienta

**setHeader()**

- nastaví header v session, používá se při změně klienta

**getData(Request $request, $tabName)** 

- $tabname - název tabu, který se má získat

- obecná metoda pro získávání dat z jendotlivých tabů


**saveData($data, $new = false, $request = false)** 

- $data - data, která se mají uložit
- $new - značí, pokud se jendá o nového klienta
- obecná metoda pro ukládání dat z jendotlivých tabů
