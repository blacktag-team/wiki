# Aplikace iFin 2 - Backend - ClientsController

Controller pro práci s klienty.

**Metody controlleru:**

- **getAll(Request $request)** - vrátí všechny klienty daného agenta
- **getAuth(Request $request)** - metoda pro data do tabu Autorizace
- **hideClient(Request $request)** - pošle požadavek na skrytí klienta
- **sendSms(Request $request)** - pošle požadavek na zaslání sms kódu pro autorizaci klienta
- **authBySms(Request $request)** - pošle požadavek na autorizaci klienta pomocí SMS kódu
- **getAuthPdf(Request $request)** - požadavek na autorizační protokol
- **sendEmail($request)** - zašle klientovi email s analýzou
- **htmlProtocol(Request $request, $step)** - získá HTML data pro autorizaci
- **getAnalysisPdf(Request $request, $name)** - získá PDF s analýzou
- **VerificationStatus(Request $request)** - zjistí verification status klienta