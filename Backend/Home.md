# Aplikace iFin 2 - Backend

Backend využívá kromě zmíněného PHP framweroku Laravel, tyto knihovny:

- sentry/sentry-laravel - knihovna pro logování chyb
- guzzlehttp/guzzle - knihovna pro odesílání HTTP requetů

Backend aplikace je vytvořen z důvodu izolace logiky pro komunikaci s externím API společnosti Business Logic. Backend by nebyl nutný, pokud by nebyla potřeba izolace, nicméně z důvodu možného výskytu vitlých dat se nejedná o tento případ.

Frontend komunikuje s externím API výhradně přes interní backend, a to odesíláním requestů na endpointy definované v routes/web.php.

Níže uvedená routa směřuje veškerý zbylý provoz na SPAController, kde poté funkci routování přebírá Frontend.

```php
Route::get('/{any}', 'SpaController@index')->where('any', '.*');
```

**Controllery**

Controllery obsluhují funckionalitu celé aplikace. Aplikace obsahuje tyto controllery:

- [GlobalController](./GlobalController.md)
- [AllPdfController](./AllPdfController.md)
- [AuthController](./AuthController.md)
- [ClientsController](./ClientsController.md)
- [FileController](./FileController.md)
- [ManagerController](./ManagerController.md)
- [SpaController](./SpaController.md)

A také controllery pro jednotlivé taby:

- FamilyController
- IncomeController
- PersonalInfoController
- RentController
- CostsController
- HousingController
- RatingController
- InvestmentController
- PriorityController
- RiskController
- ProductsController
- NoteController

Controllery pro taby obshaují vždy dvě metody:
- **get** (např. getCost, getIncome apod.) - metoda pro zsíkávání dat daného tabu, pokud data nejsou vrátí *false*
- **save** (např. saveCost, saveIncome apod.) - metoda pro uložení dat tabu


**Všechny controllery komunikují s API přes globální metody, které jsou definované v GlobalControlleru!!**