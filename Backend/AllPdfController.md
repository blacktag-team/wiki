# Aplikace iFin 2 - Backend - AllPdfController

Controller sloužící pro zpracování odkazů, které jsou zasílány klinetům emialem. Controller přebírá metody z GlobalControlleru.

**Metody controlleru nevyžadují autentizaci, jelikož k nim přistupují klienti.**

- **metoda index()** - slouží pro vykreslení stránky
- **metoda getByHash(Request $request)** - získá na základě dodaného hashe data PDF z externího API
- **sendNew(Request $request)** - pokud PDF expirovalo, může uživat kliknout na zaslat nový odkaz, což zavolaá tuto metodu a zajistí odeslání emailu s novým odkazem

