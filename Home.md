# Aplikace iFin2


Dokumentace aplikace iFin 2, která je určena jako náhrada papírové analýzy společnosti Fincentrum. Aplikace je používána finančními poradci v přímém kontaktu s klienty.

- [Frontend](./Frontend/Home.md)
- [Backend](./Backend/Home.md)

## Použité technologie

Aplikace je vytvořena použitím těchto technologií:

- [Laravel 5.6](https://laravel.com/docs/5.6/)
- [Vue2](https://vuejs.org/v2/guide/index.html)
- [Vuex](https://vuex.vuejs.org/guide/)
- [Element UI](https://github.com/ElemeFE/element)
- [Vue Chart.js](https://github.com/apertureless/vue-chartjs)
- [Sentry](https://sentry.io/welcome/)
- [GuzzleHTTP](http://docs.guzzlephp.org/en/stable/)


## API


Apliakce pro ukládání dat používá API od společnosti Business Logic s.r.o.  

Veškerá data jsou tedy uchovávána mimo samotnou aplikaci.


Have fun!